FROM ubuntu:14.04

MAINTAINER Jason L. Shiffer <jshiffer@zerotao.org>


# update ubuntu images to support/default to UTF8
RUN locale-gen en_US.UTF-8 \
    && update-locale LANG=en_US.UTF-8 LANGUAGE=en_US LC_MESSAGES=POSIX \
    && dpkg-reconfigure locales

# erlang-nox
# erlang-dev
# erlang-src

ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

RUN apt-get update \
    && runtimeDeps=' \
        unzip \
    '\
    && buildDeps=' \
        wget \
    ' \
    && corePackages=' \
        esl-erlang=1:18.3 \
        elixir=1.1.1-2 \
    ' \
    && apt-get install -y --no-install-recommends $runtimeDeps  \
    && apt-get install -y --no-install-recommends $buildDeps  \
    && apt-get install software-properties-common -y \
    && wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb \
    && dpkg -i erlang-solutions_1.0_all.deb \
    && apt-get update \
    && apt-get install --no-install-recommends -y $corePackages \
    && mix local.hex --force \
    && mix local.rebar --force \
    && mix hex.info \
    && apt-get purge -y --auto-remove $buildDeps
